====================================
CAPÍTULO II Recursos Administrativos
====================================

Artículo 217. Facultad de contradicción
=======================================

217.1 Conforme a lo señalado en el artículo 120, frente a un acto administrativo que se supone viola, desconoce o lesiona un derecho o interés legítimo, procede su contradicción en la vía administrativa mediante los recursos administrativos señalados en el artículo siguiente, iniciándose el correspondiente procedimiento recursivo.

217.2 Sólo son impugnables los actos definitivos que ponen fin a la instancia y los actos de trámite que determinen la imposibilidad de continuar el procedimiento o produzcan indefensión. La contradicción a los restantes actos de trámite deberá alegarse por los interesados para su consideración en el acto que ponga fin al procedimiento y podrán impugnarse con el recurso administrativo que, en su caso, se interponga contra el acto definitivo.

217.3 No cabe la impugnación de actos que sean reproducción de otros anteriores que hayan quedado firmes, ni la de los confirmatorios de actos consentidos por no haber sido recurridos en tiempo y forma.

217.4 Cabe la acumulación de pretensiones impugnatorias en forma subsidiaria, cuando en las instancias anteriores se haya analizado los hechos y/o fundamentos en que se sustenta la referida pretensión subsidiaria.

(Texto según el artículo 206 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 218. Recursos administrativos
======================================

218.1 Los recursos administrativos son:

a) Recurso de reconsideración

b) Recurso de apelación

Solo en caso que por ley o decreto legislativo se establezca expresamente, cabe la interposición del recurso administrativo de revisión.

218.2 El término para la interposición de los recursos es de quince (15) días perentorios, y deberán resolverse en el plazo de treinta (30) días.

(Texto según el artículo 207 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 219.- Recurso de reconsideración
=========================================

El recurso de reconsideración se interpondrá ante el mismo órgano que dictó el primer acto que es materia de la impugnación y deberá sustentarse en nueva prueba. En los casos de actos administrativos emitidos por órganos que constituyen única instancia no se requiere nueva prueba. Este recurso es opcional y su no interposición no impide el ejercicio del recurso de apelación.

(Texto según el artículo 208 de la Ley Nº 27444)

Artículo 220.- Recurso de apelación
===================================

El recurso de apelación se interpondrá cuando la impugnación se sustente en diferente interpretación de las pruebas producidas o cuando se trate de cuestiones de puro derecho, debiendo dirigirse a la misma autoridad que expidió el acto que se impugna para que eleve lo actuado al superior jerárquico.

(Texto según el artículo 209 de la Ley Nº 27444)

Artículo 221.- Requisitos del recurso
=====================================

El escrito del recurso deberá señalar el acto del que se recurre y cumplirá los demás requisitos previstos en el artículo 124.

(Texto según el artículo 211 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 222.- Acto firme
=========================

Una vez vencidos los plazos para interponer los recursos administrativos se perderá el derecho a articularlos quedando firme el acto.

(Texto según el artículo 212 de la Ley Nº 27444)

Artículo 223.- Error en la calificación
=======================================

El error en la calificación del recurso por parte del recurrente no será obstáculo para su tramitación siempre que del escrito se deduzca su verdadero carácter.

(Texto según el artículo 213 de la Ley Nº 27444)

Artículo 224.- Alcance de los recursos
======================================

Los recursos administrativos se ejercitarán por una sola vez en cada procedimiento administrativo y nunca simultáneamente.

(Texto según el artículo 214 de la Ley Nº 27444)

Artículo 225.- Silencio administrativo en materia de recursos
=============================================================

El silencio administrativo en materia de recursos se regirá por lo dispuesto por el artículo 38 y el numeral 2) del párrafo 35.1 del artículo 35.

(Texto según el artículo 215 de la Ley Nº 27444)

Artículo 226.- Suspensión de la ejecución
=========================================

226.1 La interposición de cualquier recurso, excepto los casos en que una norma legal establezca lo contrario, no suspenderá la ejecución del acto impugnado.

226.2 No obstante lo dispuesto en el numeral anterior, la autoridad a quien competa resolver el recurso suspende de oficio o a petición de parte la ejecución del acto recurrido cuando concurra alguna de las siguientes circunstancias:

a) Que la ejecución pudiera causar perjuicios de imposible o difícil reparación.

b) Que se aprecie objetivamente la existencia de un vicio de nulidad trascendente.

226.3 La decisión de la suspensión se adoptará previa ponderación suficientemente razonada entre el perjuicio que causaría al interés público o a terceros la suspensión y el perjuicio que causa al recurrente la eficacia inmediata del acto recurrido.

226.4 Al disponerse la suspensión podrán adoptarse las medidas que sean necesarias para asegurar la protección del interés público o los derechos de terceros y la eficacia de la resolución impugnada.

226.5 La suspensión se mantendrá durante el trámite del recurso administrativo o el correspondiente proceso contencioso-administrativo, salvo que la autoridad administrativa o judicial disponga lo contrario si se modifican las condiciones bajo las cuales se decidió.

(Texto según el artículo 216 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 227.- Resolución
=========================

227.1 La resolución del recurso estimará en todo o en parte o desestimará las pretensiones formuladas en el mismo o declarará su inadmisión.

227.2 Constatada la existencia de una causal de nulidad, la autoridad, además de la declaración de nulidad, resolverá sobre el fondo del asunto, de contarse con los elementos suficientes para ello. Cuando no sea posible pronunciarse sobre el fondo del asunto, se dispondrá la reposición del procedimiento al momento en que el vicio se produjo.

(Texto según el artículo 217 de la Ley Nº 27444)

Artículo 228.- Agotamiento de la vía administrativa
===================================================

228.1 Los actos administrativos que agotan la vía administrativa podrán ser impugnados ante el Poder Judicial mediante el proceso contencioso-administrativo a que se refiere el artículo 148 de la Constitución Política del Estado.

228.2 Son actos que agotan la vía administrativa:

a) El acto respecto del cual no proceda legalmente impugnación ante una autoridad u órgano jerárquicamente superior en la vía administrativa o cuando se produzca silencio administrativo negativo, salvo que el interesado opte por interponer recurso de reconsideración, en cuyo caso la resolución que se expida o el silencio administrativo producido con motivo de dicho recurso impugnativo agota la vía administrativa; o

b) El acto expedido o el silencio administrativo producido con motivo de la interposición de un recurso de apelación en aquellos casos en que se impugne el acto de una autoridad u órgano sometido a subordinación jerárquica; o

c) El acto expedido o el silencio administrativo producido con motivo de la interposición de un recurso de revisión, únicamente en los casos a que se refiere el artículo 218; o

d) El acto que declara de oficio la nulidad o revoca otros actos administrativos en los casos a que se refieren los artículos 213 y 214; o

e) Los actos administrativos de los Tribunales o Consejos Administrativos regidos por leyes especiales.

(Texto según el artículo 218 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

