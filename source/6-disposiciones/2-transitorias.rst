==========================================
DISPOSICIONES COMPLEMENTARIAS TRANSITORIAS
==========================================

Primera.- Regulación transitoria
================================

1. Los procedimientos administrativos iniciados antes de la entrada en vigor de la presente Ley, se regirán por la normativa anterior hasta su conclusión.

2. No obstante, son aplicables a los procedimientos en trámite, las disposiciones de la presente Ley que reconozcan derechos o facultades a los administrados frente a la administración, así como su Título Preliminar.

3. Los procedimientos especiales iniciados durante el plazo de adecuación contemplado en la tercera disposición transitoria se regirán por lo dispuesto en la normativa anterior que les sea de aplicación, hasta la aprobación de la modificación correspondiente, en cuyo caso los procedimientos iniciados con posterioridad a su entrada en vigor, se regulan por la citada normativa de adecuación.

(Texto según la sección de las Disposiciones Complementarias Transitorias de la Ley Nº 27444)

Segunda.- Plazo para la adecuación de procedimientos especiales
===============================================================

Reglamentariamente, en el plazo de seis meses a partir de la publicación de esta Ley, se llevará a efecto la adecuación de las normas de los entes reguladores de los distintos procedimientos administrativos, cualquiera que sea su rango, con el fin de lograr una integración de las normas generales supletoriamente aplicables.

(Texto según la sección de las Disposiciones Complementarias Transitorias de la Ley Nº 27444)

Tercera.- Plazo para la aprobación del TUPA
===========================================

Las entidades deberán aprobar su TUPA conforme a las normas de la presente Ley, en un plazo máximo de cuatro meses contados a partir de la vigencia de la misma.

(Texto según la sección de las Disposiciones Complementarias Transitorias de la Ley Nº 27444)

Cuarta.- Régimen de fedatarios
==============================

Para efectos de lo dispuesto en el artículo 138 del presente Texto Único Ordenado de la Ley Nº 27444, cada entidad podrá elaborar un reglamento interno en el cual se establecerá los requisitos, atribuciones y demás normas relacionadas con el desempeño de las funciones de fedatario.

(Texto según la sección de las Disposiciones Complementarias Transitorias de la Ley Nº 27444)

Quinta.- Difusión de la presente Ley
====================================

Las entidades, bajo responsabilidad de su titular, deberán realizar acciones de difusión, información y capacitación del contenido y alcances de la presente Ley a favor de su personal y del público usuario. Dichas acciones podrán ejecutarse a través de Internet, impresos, charlas, afiches u otros medios que aseguren la adecuada difusión de la misma. El costo de las acciones de información, difusión y capacitación no deberá ser trasladado al público usuario.

Las entidades en un plazo no mayor a los 6 (seis) meses de publicada la presente Ley, deberán informar a la Presidencia del Consejo de Ministros sobre las acciones realizadas para el cumplimiento de lo dispuesto en el párrafo anterior.

(Texto según la sección de las Disposiciones Complementarias Transitorias de la Ley Nº 27444)

Sexta
=====

Sexta.- Las entidades tendrán un plazo de sesenta (60) días, contado desde la vigencia del presente Decreto Legislativo, para adecuar sus procedimientos especiales según lo previsto en el numeral 2 del artículo II del Título Preliminar del presente Texto Único Ordenado de la Ley Nº 27444.

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Sétima
======

Sétima.- En un plazo de ciento veinte (120) días, contado desde la vigencia del presente Decreto Legislativo, las entidades deben justificar ante la Presidencia del Consejo de Ministros los procedimientos que requieren la aplicación de silencio negativo, previsto en el artículo 38 del presente Texto Único Ordenado de la Ley Nº 27444.

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Octava
======

Octava.- En un plazo de ciento veinte (120) días, contado desde la vigencia del presente Decreto Legislativo, las entidades deberán adecuar los costos de sus procedimientos administrativos y servicios prestados en exclusividad, de acuerdo a lo previsto en el numeral 53.6 del artículo 53 del presente Texto Único Ordenado de la Ley Nº 27444.

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Novena
======

Novena.- Para la aplicación de la pérdida de efectividad y ejecutoriedad del acto administrativo prevista en el numeral 204.1.2 del artículo 204 del presente Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General, se establece un plazo de seis (6) meses, contado desde la vigencia del presente Decreto Legislativo, para aquellos actos que a la fecha de entrada en vigencia del presente decreto legislativo hayan transcurrido más de dos (2) años de haber adquirido firmeza.

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Décima
======

Décima.- Para la aplicación de la caducidad prevista en el artículo 259 del presente Texto Único Ordenado de la Ley Nº 27444, Ley del Procedimiento Administrativo General, se establece un plazo de un (1) año, contado desde la vigencia del Decreto Legislativo Nº 1272, para aquellos procedimientos sancionadores que a la fecha se encuentran en trámite.

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Décimo Primera
==============

Décimo Primera.- En un plazo de sesenta (60) días hábiles, contados desde la vigencia del presente Decreto Legislativo, se aprobará el Texto Único Ordenado de la Ley Nº 27444, por Decreto Supremo refrendado por el Ministerio de Justicia y Derechos Humanos.

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Décimo Segunda
==============

Décimo Segunda.- Los documentos prohibidos de solicitar a los administrados o usuarios a los que hace referencia el artículo 5 del Decreto Legislativo 1246, Decreto Legislativo que aprueba diversas medidas de simplificación administrativa, y aquellos que se determinen mediante Decreto Supremo, conforme a lo establecido en el numeral 5.3 del referido artículo, son difundidos a través del Portal del Estado Peruano (http://www.peru.gob.pe/) y del Portal de Servicios al Ciudadano y Empresas (http://www.serviciosalciudadano.gob.pe/).

(Texto según la sección de las Disposiciones Complementarias Transitorias del Decreto Legislativo Nº 1272)

Décimo Tercera
==============

Décimo Tercera.- Casillas electrónicas o sistemas informáticos existentes o en proceso de implementación

Lo dispuesto para la notificación en casillas electrónicas o sistemas informáticos existentes o en proceso de implementación a la fecha de entrada en vigencia del presente decreto legislativo continúan operando, y en lo que resulte compatible a su funcionamiento, se adecuan a lo dispuesto por el Decreto Supremo de la Presidencia del Consejo de Ministros que apruebe los criterios, condiciones, mecanismos y plazos para la implementación gradual en las entidades públicas de la casilla única electrónica.

Asimismo, lo previsto en el quinto párrafo del numeral 20.4 de la Ley Nº 27444, Ley del Procedimiento Administrativo General, no resulta aplicable para las casillas electrónicas cuya obligatoriedad fue establecida con anterioridad al presente decreto legislativo.

(Texto según la Única Disposición Complementaria Transitoria del Decreto Legislativo Nº 1452)

