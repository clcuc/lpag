==================================================
CAPÍTULO III Eficacia de los actos administrativos
==================================================

Artículo 16.- Eficacia del acto administrativo
==============================================

16.1 El acto administrativo es eficaz a partir de que la notificación legalmente realizada produce sus efectos, conforme a lo dispuesto en el presente capítulo.

16.2 El acto administrativo que otorga beneficio al administrado se entiende eficaz desde la fecha de su emisión, salvo disposición diferente del mismo acto.

(Texto según el artículo 16 de la Ley Nº 27444)

Artículo 17.- Eficacia anticipada del acto administrativo
=========================================================

17.1 La autoridad podrá disponer en el mismo acto administrativo que tenga eficacia anticipada a su emisión, sólo si fuera más favorable a los administrados, y siempre que no lesione derechos fundamentales o intereses de buena fe legalmente protegidos a terceros y que existiera en la fecha a la que pretenda retrotraerse la eficacia del acto el supuesto de hecho justificativo para su adopción.

17.2 También tienen eficacia anticipada la declaratoria de nulidad y los actos que se dicten en enmienda.

(Texto según el artículo 17 de la Ley Nº 27444)

Artículo 18.- Obligación de notificar
=====================================

18.1 La notificación del acto es practicada de oficio y su debido diligenciamiento es competencia de la entidad que lo dictó. La notificación debe realizarse en día y hora hábil, salvo regulación especial diferente o naturaleza continuada de la actividad.

18.2 La notificación personal podrá ser efectuada a través de la propia entidad, por servicios de mensajería especialmente contratados para el efecto y en caso de zonas alejadas, podrá disponerse se practique por intermedio de las autoridades políticas del ámbito local del administrado.

(Texto modificado según el artículo 2 del Decreto Legislativo Nº 1272)

Artículo 19.- Dispensa de notificación
======================================

19.1 La autoridad queda dispensada de notificar formalmente a los administrados cualquier acto que haya sido emitido en su presencia, siempre que exista acta de esta actuación procedimental donde conste la asistencia del administrado.

19.2 También queda dispensada de notificar si el administrado tomara conocimiento del acto respectivo mediante su acceso directo y espontáneo al expediente, recabando su copia, dejando constancia de esta situación en el expediente.

(Texto según el artículo 19 de la Ley Nº 27444)

Artículo 20. Modalidades de notificación
========================================

20.1 Las notificaciones son efectuadas a través de las siguientes modalidades, según este respectivo orden de prelación:

20.1.1 Notificación personal al administrado interesado o afectado por el acto, en su domicilio.

20.1.2 Mediante telegrama, correo certificado, telefax; o cualquier otro medio que permita comprobar fehacientemente su acuse de recibo y quien lo recibe, siempre que el empleo de cualquiera de estos medios hubiese sido solicitado expresamente por el administrado.

20.1.3 Por publicación en el Diario Oficial o en uno de los diarios de mayor circulación en el territorio nacional, salvo disposición distinta de la ley. Adicionalmente, la autoridad competente dispone la publicación del acto en el respectivo Portal Institucional, en caso la entidad cuente con este mecanismo.

(Texto según numeral 20.1 del artículo 20 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

20.2 La autoridad no puede suplir alguna modalidad con otra ni modificar el orden de prelación establecido en el numeral anterior, bajo sanción de nulidad de la notificación. Puede acudir complementariamente a aquellas u otras, si así lo estime conveniente para mejorar las posibilidades de participación de los administrados.

20.3 Tratamiento igual al previsto en este capítulo corresponde a los citatorios, los emplazamientos, los requerimientos de documentos o de otros actos administrativos análogos.

20.4. El administrado interesado o afectado por el acto que hubiera consignado en su escrito alguna dirección electrónica que conste en el expediente puede ser notificado a través de ese medio siempre que haya dado su autorización expresa para ello. Para este caso no es de aplicación el orden de prelación dispuesto en el numeral 20.1.

La notificación dirigida a la dirección de correo electrónico señalada por el administrado se entiende válidamente efectuada cuando la entidad reciba la respuesta de recepción de la dirección electrónica señalada por el administrado o esta sea generada en forma automática por una plataforma tecnológica o sistema informático que garantice que la notificación ha sido efectuada. La notificación surte efectos el día que conste haber sido recibida, conforme lo previsto en el numeral 2 del artículo 25.

En caso de no recibirse respuesta automática de recepción en un plazo máximo de dos (2) días hábiles contados desde el día siguiente de efectuado el acto de notificación vía correo electrónico, se procede a notificar por cédula conforme al inciso 20.1.1, volviéndose a computar el plazo establecido en el numeral 24.1 del artículo 24.

Para la notificación por correo electrónico, la autoridad administrativa, si lo considera pertinente, puede emplear firmas y certificados digitales conforme a lo estipulado en la ley de la materia.

La entidad que cuente con disponibilidad tecnológica puede asignar al administrado una casilla electrónica gestionada por esta, para la notificación de actos administrativos, así como actuaciones emitidas en el marco de cualquier actividad administrativa, siempre que cuente con el consentimiento expreso del administrado. Mediante decreto supremo del sector, previa opinión favorable de la Presidencia del Consejo de Ministros y el Ministerio de Justicia y Derechos Humanos, puede aprobar la obligatoriedad de la notificación vía casilla electrónica.

En ese caso, la notificación se entiende válidamente efectuada cuando la entidad la deposite en el buzón electrónico asignado al administrado, surtiendo efectos el día que conste haber sido recibida, conforme a lo previsto en el numeral 2 del artículo 25.

Asimismo, se establece la implementación de la casilla única electrónica para las comunicaciones y notificaciones de las entidades del Estado dirigidas a los administrados. Mediante Decreto Supremo refrendado por la Presidencia del Consejo de Ministros se aprueban los criterios, condiciones, mecanismos y plazos para la implementación gradual en las entidades públicas de la casilla única electrónica.

"El consentimiento expreso a que se refiere el quinto párrafo del numeral 20.4 de la presente Ley puede ser otorgado por vía electrónica.”(*)

(Texto según numeral 20.4 del artículo 20 de la Ley Nº 27444, modificado según el artículo 2 del Decreto Legislativo Nº 1452)

(*) De conformidad con el Artículo 3 del Decreto Legislativo N° 1497, publicado el 10 mayo 2020, se incorpora un último párrafo en el artículo 20 de la Ley Nº 27444, Ley del Procedimiento Administrativo General.

Artículo 21.- Régimen de la notificación personal
=================================================

21.1 La notificación personal se hará en el domicilio que conste en el expediente, o en el último domicilio que la persona a quien deba notificar haya señalado ante el órgano administrativo en otro procedimiento análogo en la propia entidad dentro del último año.

21.2 En caso que el administrado no haya indicado domicilio, o que éste sea inexistente, la autoridad deberá emplear el domicilio señalado en el Documento Nacional de Identidad del administrado. De verificar que la notificación no puede realizarse en el domicilio señalado en el Documento Nacional de Identidad por presentarse alguna de las circunstancias descritas en el numeral 23.1.2 del artículo 23, se deberá proceder a la notificación mediante publicación.

21.3 En el acto de notificación personal debe entregarse copia del acto notificado y señalar la fecha y hora en que es efectuada, recabando el nombre y firma de la persona con quien se entienda la diligencia. Si ésta se niega a firmar o recibir copia del acto notificado, se hará constar así en el acta, teniéndose por bien notificado. En este caso la notificación dejará constancia de las características del lugar donde se ha notificado.

21.4 La notificación personal, se entenderá con la persona que deba ser notificada o su representante legal, pero de no hallarse presente cualquiera de los dos en el momento de entregar la notificación, podrá entenderse con la persona que se encuentre en dicho domicilio, dejándose constancia de su nombre, documento de identidad y de su relación con el administrado.

21.5 En el caso de no encontrar al administrado u otra persona en el domicilio señalado en el procedimiento, el notificador deberá dejar constancia de ello en el acta y colocar un aviso en dicho domicilio indicando la nueva fecha en que se hará efectiva la siguiente notificación. Si tampoco pudiera entregar directamente la notificación en la nueva fecha, se dejará debajo de la puerta un acta conjuntamente con la notificación, copia de los cuales serán incorporados en el expediente.

(Texto según el artículo 21 de la Ley Nº 27444)

Artículo 22.- Notificación a pluralidad de interesados
======================================================

22.1 Cuando sean varios sus destinatarios, el acto será notificado personalmente a todos, salvo sí actúan unidos bajo una misma representación o si han designado un domicilio común para notificaciones, en cuyo caso éstas se harán en dicha dirección única.

22.2 Si debiera notificarse a más de diez personas que han planteado una sola solicitud con derecho común, la notificación se hará con quien encabeza el escrito inicial, indicándole que trasmita la decisión a sus cointeresados.

(Texto según el artículo 22 de la Ley Nº 27444)

Artículo 23.- Régimen de publicación de actos administrativos
=============================================================

23.1 La publicación procederá conforme al siguiente orden:

23.1.1 En vía principal, tratándose de disposiciones de alcance general o aquellos actos administrativos que interesan a un número indeterminado de administrados no apersonados al procedimiento y sin domicilio conocido.

23.1.2 En vía subsidiaria a otras modalidades, tratándose de actos administrativos de carácter particular cuando la ley así lo exija, o la autoridad se encuentre frente a alguna de las siguientes circunstancias evidenciables e imputables al administrado:

- Cuando resulte impracticable otra modalidad de notificación preferente por ignorarse el domicilio del administrado, pese a la indagación realizada.

- Cuando se hubiese practicado infructuosamente cualquier otra modalidad, sea porque la persona a quien deba notificarse haya desaparecido, sea equivocado el domicilio aportado por el administrado o se encuentre en el extranjero sin haber dejado representante legal, pese al requerimiento efectuado a través del Consulado respectivo.

23.2 La publicación de un acto debe contener los mismos elementos previstos para la notificación señalados en este capítulo; pero en el caso de publicar varios actos con elementos comunes, se podrá proceder en forma conjunta con los aspectos coincidentes, especificándose solamente lo individual de cada acto.

(Texto según el artículo 23 de la Ley Nº 27444)

23.3. Excepcionalmente, se puede realizar la publicación de un acto siempre que contenga los elementos de identificación del acto administrativo y la sumilla de la parte resolutiva y que se direccione al Portal Institucional de la autoridad donde se publica el acto administrativo en forma íntegra, surtiendo efectos en un plazo de 5 días contados desde la publicación. Asimismo, la administración pública, en caso sea solicitada por el administrado destinatario del acto, está obligada a entregar copia de dicho acto administrativo. La primera copia del acto administrativo es gratuita y debe ser emitida y entregada en el mismo día que es solicitada, y por razones excepcionales debidamente justificadas, en el siguiente día hábil. Mediante Decreto Supremo del Ministerio de Justicia y Derechos Humanos se establecen los lineamentos para la publicación de este tipo de actos.

(Numeral incorporado según el artículo 3 del Decreto Legislativo Nº 1452)

Artículo 24.- Plazo y contenido para efectuar la notificación
=============================================================

24.1 Toda notificación deberá practicarse a más tardar dentro del plazo de cinco (5) días, a partir de la expedición del acto que se notifique, y deberá contener:

24.1.1 El texto íntegro del acto administrativo, incluyendo su motivación.

24.1.2 La identificación del procedimiento dentro del cual haya sido dictado.

24.1.3 La autoridad e institución de la cual procede el acto y su dirección.

24.1.4 La fecha de vigencia del acto notificado, y con la mención de si agotare la vía administrativa.

24.1.5 Cuando se trate de una publicación dirigida a terceros, se agregará además cualquier otra información que pueda ser importante para proteger sus intereses y derechos.

24.1.6 La expresión de los recursos que proceden, el órgano ante el cual deben presentarse los recurso(*)NOTA SPIJ y el plazo para interponerlos.

24.2 Si en base a información errónea, contenida en la notificación, el administrado practica algún acto procedimental que sea rechazado por la entidad, el tiempo transcurrido no será tomado en cuenta para determinar el vencimiento de los plazos que correspondan.

(Texto según el artículo 24 de la Ley Nº 27444)

Artículo 25.- Vigencia de las notificaciones
============================================

Las notificaciones surtirán efectos conforme a las siguientes reglas:

1. Las notificaciones personales: el día que hubieren sido realizadas.

2. Las cursadas mediante correo certificado, oficio, correo electrónico y análogos: el día que conste haber sido recibidas.

CONCORDANCIAS:     D.U. N° 038-2020, Segunda Disp. Comp. Final (Notificación electrónica obligatoria durante el Estado de Emergencia Nacional y la Emergencia Sanitaria)

3. Las notificaciones por publicaciones: a partir del día de la última publicación en el Diario Oficial.

4. Cuando por disposición legal expresa, un acto administrativo deba ser a la vez notificado personalmente al administrado y publicado para resguardar derechos o intereses legítimos de terceros no apersonados o indeterminados, el acto producirá efectos a partir de la última notificación.

Para efectos de computar el inicio de los plazos se deberán seguir las normas establecidas en el artículo 144, con excepción de la notificación de medidas cautelares o precautorias, en cuyo caso deberá aplicarse lo dispuesto en los numerales del párrafo precedente.

(Texto según el artículo 25 de la Ley Nº 27444)

Artículo 26.- Notificaciones defectuosas
========================================

26.1 En caso que se demuestre que la notificación se ha realizado sin las formalidades y requisitos legales, la autoridad ordenará se rehaga, subsanando las omisiones en que se hubiesen incurrido, sin perjuicio para el administrado.

26.2 La desestimación del cuestionamiento a la validez de una notificación, causa que dicha notificación opere desde la fecha en que fue realizada.

(Texto según el artículo 26 de la Ley Nº 27444)

Artículo 27.- Saneamiento de notificaciones defectuosas
=======================================================

27.1 La notificación defectuosa por omisión de alguno de sus requisitos de contenido, surtirá efectos legales a partir de la fecha en que el interesado manifiesta expresamente haberla recibido, si no hay prueba en contrario.

27.2 También se tendrá por bien notificado al administrado a partir de la realización de actuaciones procedimentales del interesado que permitan suponer razonablemente que tuvo conocimiento oportuno del contenido o alcance de la resolución, o interponga cualquier recurso que proceda. No se considera tal, la solicitud de notificación realizada por el administrado, a fin que le sea comunicada alguna decisión de la autoridad.

(Texto según el artículo 27 de la Ley Nº 27444)

Artículo 28.- Comunicaciones al interior de la administración
=============================================================

28.1 Las comunicaciones entre los órganos administrativos al interior de una entidad serán efectuadas directamente, evitando la intervención de otros órganos.

28.2 Las comunicaciones de resoluciones a otras autoridades nacionales o el requerimiento para el cumplimiento de diligencias en el procedimiento serán cursadas siempre directamente bajo el régimen de la notificación sin actuaciones de mero traslado en razón de jerarquías internas ni transcripción por órganos intermedios.

28.3 Cuando alguna otra autoridad u órgano administrativo interno deba tener conocimiento de la comunicación se le enviará copia informativa.

28.4 La constancia documental de la transmisión a distancia por medios electrónicos entre entidades y autoridades, constituye de por sí documentación auténtica y dará plena fe a todos sus efectos dentro del expediente para ambas partes, en cuanto a la existencia del original transmitido y su recepción.

(Texto según el artículo 28 de la Ley Nº 27444)

