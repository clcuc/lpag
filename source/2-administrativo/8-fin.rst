===================================
CAPÍTULO VIII Fin del Procedimiento
===================================

Artículo 197.- Fin del procedimiento
====================================

197.1 Pondrán fin al procedimiento las resoluciones que se pronuncian sobre el fondo del asunto, el silencio administrativo positivo, el silencio administrativo negativo en el caso a que se refiere el párrafo 199.4 del artículo 199, el desistimiento, la declaración de abandono, los acuerdos adoptados como consecuencia de conciliación o transacción extrajudicial que tengan por objeto poner fin al procedimiento y la prestación efectiva de lo pedido a conformidad del administrado en caso de petición graciable.

197.2 También pondrá fin al procedimiento la resolución que así lo declare por causas sobrevenidas que determinen la imposibilidad de continuarlo.

(Texto según el artículo 186 de la Ley Nº 27444)

Artículo 198.- Contenido de la resolución
=========================================

198.1 La resolución que pone fin al procedimiento cumplirá los requisitos del acto administrativo señalados en el Capítulo Primero del Título Primero de la presente Ley.

198.2 En los procedimientos iniciados a petición del interesado, la resolución será congruente con las peticiones formuladas por éste, sin que en ningún caso pueda agravar su situación inicial y sin perjuicio de la potestad de la administración de iniciar de oficio un nuevo procedimiento, si procede.

(Texto según el artículo 187 de la Ley Nº 27444)

Artículo 199.- Efectos del silencio administrativo
==================================================

199.1. Los procedimientos administrativos sujetos a silencio administrativo positivo quedarán automáticamente aprobados en los términos en que fueron solicitados si transcurrido el plazo establecido o máximo, al que se adicionará el plazo máximo señalado en el numeral 24.1 del artículo 24, la entidad no hubiere notificado el pronunciamiento respectivo. La declaración jurada a la que se refiere el artículo 37 no resulta necesaria para ejercer el derecho resultante del silencio administrativo positivo ante la misma entidad.

199.2 El silencio positivo tiene para todos los efectos el carácter de resolución que pone fin al procedimiento, sin perjuicio de la potestad de nulidad de oficio prevista en el artículo 213.

199.3 El silencio administrativo negativo tiene por efecto habilitar al administrado la interposición de los recursos administrativos y acciones judiciales pertinentes.

199.4 Aun cuando opere el silencio administrativo negativo, la administración mantiene la obligación de resolver, bajo responsabilidad, hasta que se le notifique que el asunto ha sido sometido a conocimiento de una autoridad jurisdiccional o el administrado haya hecho uso de los recursos administrativos respectivos.

199.5 El silencio administrativo negativo no inicia el cómputo de plazos ni términos para su impugnación.

199.6. En los procedimientos sancionadores, los recursos administrativos destinados a impugnar la imposición de una sanción estarán sujetos al silencio administrativo negativo. Cuando el administrado haya optado por la aplicación del silencio administrativo negativo, será de aplicación el silencio administrativo positivo en las siguientes instancias resolutivas.

(Texto según el artículo 188 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 200.- Desistimiento del procedimiento o de la pretensión
=================================================================

200.1 El desistimiento del procedimiento importará la culminación del mismo, pero no impedirá que posteriormente vuelva a plantearse igual pretensión en otro procedimiento.

200.2 El desistimiento de la pretensión impedirá promover otro procedimiento por el mismo objeto y causa.

200.3 El desistimiento sólo afectará a quienes lo hubieren formulado.

200.4 El desistimiento podrá hacerse por cualquier medio que permita su constancia y señalando su contenido y alcance. Debe señalarse expresamente si se trata de un desistimiento de la pretensión o del procedimiento. Si no se precisa, se considera que se trata de un desistimiento del procedimiento.

200.5 El desistimiento se puede realizar en cualquier momento antes de que se notifique la resolución final que agote la vía administrativa.

200.6 La autoridad aceptará de plano el desistimiento y declarará concluido el procedimiento, salvo que, habiéndose apersonado en el mismo terceros interesados, instasen éstos su continuación en el plazo de diez días desde que fueron notificados del desistimiento.

200.7 La autoridad podrá continuar de oficio el procedimiento si del análisis de los hechos considera que podría estarse afectando intereses de terceros o la acción suscitada por la iniciación del procedimiento extrañase interés general. En ese caso, la autoridad podrá limitar los efectos del desistimiento al interesado y continuará el procedimiento.

(Texto según el artículo 189 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 201.- Desistimiento de actos y recursos administrativos
================================================================

201.1 El desistimiento de algún acto realizado en el procedimiento puede realizarse antes de que haya producido efectos.

201.2 Puede desistirse de un recurso administrativo antes de que se notifique la resolución final en la instancia, determinando que la resolución impugnada quede firme, salvo que otros administrados se hayan adherido al recurso, en cuyo caso sólo tendrá efecto para quien lo formuló.

(Texto según el artículo 190 de la Ley Nº 27444)

Artículo 202.- Abandono en los procedimientos iniciados a solicitud del administrado
====================================================================================

En los procedimientos iniciados a solicitud de parte, cuando el administrado incumpla algún trámite que le hubiera sido requerido que produzca su paralización por treinta días, la autoridad de oficio o a solicitud del administrado declarará el abandono del procedimiento. Dicha resolución deberá ser notificada y contra ella procederán los recursos administrativos pertinentes.

(Texto según el artículo 191 de la Ley Nº 27444)

