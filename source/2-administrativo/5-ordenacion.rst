=======================================
CAPÍTULO V Ordenación del Procedimiento
=======================================

Artículo 155.- Unidad de vista
==============================

Los procedimientos administrativos se desarrollan de oficio, de modo sencillo y eficaz sin reconocer formas determinadas, fases procesales, momentos procedimentales rígidos para realizar determinadas actuaciones o responder a precedencia entre ellas, salvo disposición expresa en contrario de la ley en procedimientos especiales.

(Texto según el artículo 144 de la Ley Nº 27444)

Artículo 156.- Impulso del procedimiento
========================================

La autoridad competente, aun sin pedido de parte, debe promover toda actuación que fuese necesaria para su tramitación, superar cualquier obstáculo que se oponga a regular tramitación del procedimiento; determinar la norma aplicable al caso aun cuando no haya sido invocada o fuere errónea la cita legal; así como evitar el entorpecimiento o demora a causa de diligencias innecesarias o meramente formales, adoptando las medidas oportunas para eliminar cualquier irregularidad producida.

(Texto según el artículo 145 de la Ley Nº 27444)

Artículo 157.- Medidas cautelares
=================================

157.1 Iniciado el procedimiento, la autoridad competente mediante decisión motivada y con elementos de juicio suficientes puede adoptar, provisoriamente bajo su responsabilidad, las medidas cautelares establecidas en esta Ley u otras disposiciones jurídicas aplicables, mediante decisión fundamentada, si hubiera posibilidad de que sin su adopción se arriesga la eficacia de la resolución a emitir.

157.2 Las medidas cautelares podrán ser modificadas o levantadas durante el curso del procedimiento, de oficio o a instancia de parte, en virtud de circunstancias sobrevenidas o que no pudieron ser consideradas en el momento de su adopción.

157.3 Las medidas caducan de pleno derecho cuando se emite la resolución que pone fin al procedimiento, cuando haya transcurrido el plazo fijado para su ejecución, o para la emisión de la resolución que pone fin al procedimiento.

157.4 No se podrán dictar medidas que puedan causar perjuicio de imposible reparación a los administrados.

(Texto según el artículo 146 de la Ley Nº 27444)

Artículo 158.- Cuestiones distintas al asunto principal
=======================================================

158.1 Las cuestiones que planteen los administrados durante la tramitación del procedimiento sobre extremos distintos al asunto principal, no suspenden su avance, debiendo ser resueltas en la resolución final de la instancia, salvo disposición expresa en contrario de la ley.

158.2 Tales cuestiones, para que se sustancien conjuntamente con el principal, pueden plantearse y argumentarse antes del alegato. Transcurrido este momento, se pueden hacer valer exclusivamente en el recurso.

158.3 Cuando la ley dispone una decisión anticipada sobre las cuestiones, para efectos de su impugnación, la resolución dictada en estas condiciones se considera provisional en relación con el acto final.

158.4 Serán rechazados de plano los planteamientos distintos al asunto de fondo que a criterio del instructor no se vinculen a la validez de actos procedimentales, al debido proceso o que no sean conexos a la pretensión, sin perjuicio de que el administrado pueda plantear la cuestión al recurrir contra la resolución que concluya la instancia.

(Texto según el artículo 147 de la Ley Nº 27444)

Artículo 159.- Reglas para la celeridad
=======================================

Para asegurar el cumplimiento del principio de celeridad de los procedimientos, se observan las siguientes reglas:

1. En el impulso y tramitación de casos de una misma naturaleza, se sigue rigurosamente el orden de ingreso, y se resuelven conforme lo vaya permitiendo su estado, dando cuenta al superior de los motivos de demora en el cumplimiento de los plazos de ley, que no puedan ser removidos de oficio.

2. En una sola decisión se dispondrá el cumplimiento de todos los trámites necesarios que por su naturaleza corresponda, siempre y cuando no se encuentren entre sí sucesivamente subordinados en su cumplimiento, y se concentrarán en un mismo acto todas las diligencias y actuaciones de pruebas posibles, procurando que el desarrollo del procedimiento se realice en el menor número de actos procesales.

3. Al solicitar trámites a ser efectuados por otras autoridades o los administrados, debe consignarse con fecha cierta el término final para su cumplimiento, así como el apercibimiento, de estar previsto en la normativa.

4. En ningún caso podrá afectarse la tramitación de los expedientes o la atención del servicio por la ausencia, ocasional o no, de cualquier autoridad. Las autoridades que por razones de licencia, vacaciones u otros motivos temporales o permanentes se alejen de su centro de trabajo, entregarán a quien lo sustituya o al superior jerárquico, los documentos y expedientes a su cargo, con conocimiento de los administrados.

5. Cuando sea idéntica la motivación de varias resoluciones, se podrán usar medios de producción en serie, siempre que no lesione las garantías jurídicas de los administrados; sin embargo, se considerará cada uno como acto independiente.

6. La autoridad competente, para impulsar el procedimiento, puede encomendar a algún subordinado inmediato la realización de diligencias específicas de impulso, o solicitar la colaboración de otra autoridad para su realización. En los órganos colegiados, dicha acción debe recaer en uno de sus miembros.

7. En ningún caso la autoridad podrá alegar deficiencias del administrado no advertidas a la presentación de la solicitud, como fundamento para denegar su pretensión.

(Texto según el artículo 148 de la Ley Nº 27444)

Artículo 160.- Acumulación de procedimientos
============================================

La autoridad responsable de la instrucción, por propia iniciativa o a instancia de los administrados, dispone mediante resolución irrecurrible la acumulación de los procedimientos en trámite que guarden conexión.

(Texto según el artículo 149 de la Ley Nº 27444)

Artículo 161.- Regla de expediente único
========================================

161.1 Sólo puede organizarse un expediente para la solución de un mismo caso, para mantener reunidas todas las actuaciones para resolver.

161.2 Cuando se trate de solicitud referida a una sola pretensión, se tramitará un único expediente e intervendrá y resolverá una autoridad, que recabará de los órganos o demás autoridades los informes, autorizaciones y acuerdos que sean necesarios, sin prejuicio del derecho de los administrados a instar por sí mismos los trámites pertinentes y a aportar los documentos pertinentes.

(Texto según el artículo 150 de la Ley Nº 27444)

Artículo 162.- Información documental
=====================================

Los documentos, actas, formularios y expedientes administrativos, se uniforman en su presentación para que cada especie o tipo de los mismos reúnan características iguales.

(Texto según el artículo 151 de la Ley Nº 27444)

Artículo 163.- Presentación externa de expedientes
==================================================

163.1 Los expedientes son compaginados siguiendo el orden regular de los documentos que lo integran, formando cuerpos correlativos que no excedan de doscientos folios, salvo cuando tal límite obligara a dividir escritos o documentos que constituyan un solo texto, en cuyo caso se mantendrá su unidad.

163.2 Todas las actuaciones deben foliarse, manteniéndose así durante su tramitación. Los expedientes que se incorporan a otros no continúan su foliatura, dejándose constancia de su agregación y su cantidad de fojas.

(Texto según el artículo 152 de la Ley Nº 27444)

Artículo 164.- Intangibilidad del expediente
============================================

164.1 El contenido del expediente es intangible, no pudiendo introducirse enmendaduras, alteraciones, entrelineados ni agregados en los documentos, una vez que hayan sido firmados por la autoridad competente. De ser necesarias, deberá dejarse constancia expresa y detallada de las modificaciones introducidas.

164.2 Los desgloses pueden solicitarse verbalmente y son otorgados bajo constancia del instructor y del solicitante, indicando fecha y folios, dejando una copia autenticada en el lugar correspondiente, con la foliatura respectiva.

164.3 Las entidades podrán emplear tecnología de microformas y medios informáticos para el archivo y tramitación de expedientes, previendo las seguridades, inalterabilidad e integridad de su contenido, de conformidad con la normatividad de la materia.

164.4 Si un expediente se extraviara, la administración tiene la obligación, bajo responsabilidad de reconstruir el mismo, independientemente de la solicitud del interesado, para tal efecto se aplicarán, en lo que le fuera aplicable, las reglas contenidas en el artículo 140 del Código Procesal Civil.

(Texto según el artículo 153 de la Ley Nº 27444)

Artículo 165.- Empleo de formularios
====================================

165.1 Las entidades disponen el empleo de formularios de libre reproducción y distribución gratuita, mediante los cuales los administrados, o algún servidor a su pedido, completando datos o marcando alternativas planteadas proporcionan la información usual que se estima suficiente, sin necesidad de otro documento de presentación. Particularmente se emplea cuando los administrados deban suministrar información para cumplir exigencias legales y en los procedimientos de aprobación automática.

165.2 También son utilizados cuando las autoridades deben resolver una serie numerosa de expedientes homogéneos, así como para las actuaciones y resoluciones recurrentes, que sean autorizadas previamente.

(Texto según el artículo 154 de la Ley Nº 27444)

Artículo 166.- Modelos de escritos recurrentes
==============================================

166.1 A título informativo, las entidades ponen a disposición de los administrados modelos de los escritos de empleo más recurrente en sus servicios.

166.2 En ningún caso se considera obligatoria la sujeción a estos modelos, ni su empleo puede ocasionar consecuencias adversas para quien los utilice.

(Texto según el artículo 155 de la Ley Nº 27444)

Artículo 167. Elaboración de actas
==================================

167.1 Las declaraciones de los administrados, testigos y peritos son documentadas en un acta, cuya elaboración sigue las siguientes reglas:

1. El acta indica el lugar, fecha, nombres de los partícipes, objeto de la actuación y otras circunstancias relevantes, debiendo ser formulada, leída y firmada inmediatamente después de la actuación, por los declarantes, la autoridad administrativa y por los partícipes que quisieran hacer constar su manifestación.

2. Cuando las declaraciones o actuaciones fueren grabadas, por consenso entre la autoridad y los administrados, el acta puede ser concluida dentro del quinto día del acto, o de ser el caso, antes de la decisión final.

3. Los administrados pueden dejar constancia en el acta de las observaciones que estimen necesarias sobre lo acontecido durante la diligencia correspondiente.

167.2 En los procedimientos administrativos de fiscalización y supervisión, los administrados, además, pueden ofrecer pruebas respecto de los hechos documentados en el acta.

(Texto según el artículo 156 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 168.- Medidas de seguridad documental
==============================================

Las entidades aplicarán las siguientes medidas de seguridad documental:

1. Establecer un sistema único de identificación de todos los escritos y documentos ingresados a ella, que comprenda la numeración progresiva y la fecha, así como guardará una numeración invariable para cada expediente, que será conservada a través de todas las actuaciones sucesivas, cualquiera fueran los órganos o autoridades del organismo que interviene.

2. Guardar las constancias de notificación, publicación o entrega de información sobre los actos, acuse de recibo y todos los documentos necesarios para acreditar la realización de las diligencias, con la certificación del instructor sobre su debido cumplimiento.

3. En la carátula debe consignarse el órgano y el nombre de la autoridad, con la responsabilidad encargada del trámite y la fecha del término final para la atención del expediente.

4. En ningún caso se hará un doble o falso expediente.

(Texto según el artículo 157 de la Ley Nº 27444)

Artículo 169.- Queja por defectos de tramitación
================================================

169.1 En cualquier momento, los administrados pueden formular queja contra los defectos de tramitación y, en especial, los que supongan paralización, infracción de los plazos establecidos legalmente, incumplimiento de los deberes funcionales u omisión de trámites que deben ser subsanados antes de la resolución definitiva del asunto en la instancia respectiva.

169.2 La queja se presenta ante el superior jerárquico de la autoridad que tramita el procedimiento, citándose el deber infringido y la norma que lo exige. La autoridad superior resuelve la queja dentro de los tres días siguientes, previo traslado al quejado, a fin de que pueda presentar el informe que estime conveniente al día siguiente de solicitado.

169.3 En ningún caso se suspenderá la tramitación del procedimiento en que se haya presentado queja, y la resolución será irrecurrible.

169.4 La autoridad que conoce de la queja puede disponer motivadamente que otro funcionario de similar jerarquía al quejado, asuma el conocimiento del asunto.

169.5 En caso de declararse fundada la queja, se dictarán las medidas correctivas pertinentes respecto del procedimiento, y en la misma resolución se dispondrá el inicio de las actuaciones necesarias para sancionar al responsable.

(Texto según el artículo 158 de la Ley Nº 27444)

