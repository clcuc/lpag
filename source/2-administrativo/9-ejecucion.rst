=====================================
CAPÍTULO IX Ejecución de resoluciones
=====================================

Artículo 203.- Ejecutoriedad del acto administrativo
====================================================

Los actos administrativos tendrán carácter ejecutario, salvo disposición legal expresa en contrario, mandato judicial o que estén sujetos a condición o plazo conforme a ley.

(Texto según el artículo 192 de la Ley Nº 27444)

Artículo 204.- Pérdida de ejecutoriedad del acto administrativo
===============================================================

204.1 Salvo norma expresa en contrario, los actos administrativos pierden efectividad y ejecutoriedad en los siguientes casos:

204.1.1 Por suspensión provisional conforme a ley.

204.1.2 Cuando transcurridos dos (2) años de adquirida firmeza, la administración no ha iniciado los actos que le competen para ejecutarlos.

204.1.3 Cuando se cumpla la condición resolutiva a que estaban sujetos de acuerdo a ley.

204.2 Cuando el administrado oponga al inicio de la ejecución del acto administrativo la pérdida de su ejecutoriedad, la cuestión es resuelta de modo irrecurrible en sede administrativa por la autoridad inmediata superior, de existir, previo informe legal sobre la materia.

(Texto según el artículo 193 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 205.- Ejecución forzosa
================================

Para proceder a la ejecución forzosa de actos administrativos a través de sus propios órganos competentes, o de la Policía Nacional del Perú, la autoridad cumple las siguientes exigencias:

1. Que se trate de una obligación de dar, hacer o no hacer, establecida a favor de la entidad.

2. Que la prestación sea determinada por escrito de modo claro e íntegro.

3. Que tal obligación derive del ejercicio de una atribución de imperio de la entidad o provenga de una relación de derecho público sostenida con la entidad.

4. Que se haya requerido al administrado el cumplimiento espontáneo de la prestación, bajo apercibimiento de iniciar el medio coercitivo específicamente aplicable.

5. Que no se trate de acto administrativo que la Constitución o la ley exijan la intervención del Poder Judicial para su ejecución.

6. En el caso de procedimientos trilaterales, las resoluciones finales que ordenen medidas correctivas constituyen títulos de ejecución conforme a lo dispuesto en el artículo 713 inciso 4) del Código Procesal Civil, modificado por la Ley Nº 28494, una vez que el acto quede firme o se haya agotado la vía administrativa.

En caso de resoluciones finales que ordenen medidas correctivas, la legitimidad para obrar en los procesos civiles de ejecución corresponde a las partes involucradas.

(Texto según el artículo 194 de la Ley Nº 27444)

Artículo 206.- Notificación de acto de inicio de ejecución
==========================================================

206.1 La decisión que autorice la ejecución administrativa será notificada a su destinatario antes de iniciarse la misma.

206.2 La autoridad puede notificar el inicio de la ejecución sucesivamente a la notificación del acto ejecutado, siempre que se facilite al administrado cumplir espontáneamente la prestación a su cargo.

(Texto según el artículo 195 de la Ley Nº 27444)

Artículo 207.- Medios de ejecución forzosa
==========================================

207.1 La ejecución forzosa por la entidad se efectuará respetando siempre el principio de razonabilidad, por los siguientes medios:

a) Ejecución coactiva

b) Ejecución subsidiaria

c) Multa coercitiva

d) Compulsión sobre las personas

207.2 Si fueran varios los medios de ejecución aplicables, se elegirá el menos restrictivo de la libertad individual.

207.3 Si fuese necesario ingresar al domicilio o a la propiedad del afectado, deberá seguirse lo previsto por el inciso 9) del artículo 20 de la Constitución Política del Perú.

(Texto según el artículo 196 de la Ley Nº 27444)

Artículo 208.- Ejecución coactiva
=================================

Si la entidad hubiera de procurarse la ejecución de una obligación de dar, hacer o no hacer, se seguirá el procedimiento previsto en las leyes de la materia.

(Texto según el artículo 197 de la Ley Nº 27444)

Artículo 209.- Ejecución subsidiaria
====================================

Habrá lugar a la ejecución subsidiaria cuando se trate de actos que por no ser personalísimos puedan ser realizados por sujeto distinto del obligado:

1. En este caso, la entidad realizará el acto, por sí o a través de las personas que determine, a costa del obligado.

2. El importe de los gastos, daños y perjuicios se exigirá conforme a lo dispuesto en el artículo anterior.

3. Dicho importe podrá liquidarse de forma provisional y realizarse antes de la ejecución, o reservarse a la liquidación definitiva.

(Texto según el artículo 198 de la Ley Nº 27444)

Artículo 210.- Multa coercitiva
===============================

210.1 Cuando así lo autoricen las leyes, y en la forma y cuantía que éstas determinen, la entidad puede, para la ejecución de determinados actos, imponer multas coercitivas, reiteradas por períodos suficientes para cumplir lo ordenado, en los siguientes supuestos:

a) Actos personalísimos en que no proceda la compulsión sobre la persona del obligado.

b) Actos en que, procediendo la compulsión, la administración no la estimara conveniente.

c) Actos cuya ejecución pueda el obligado encargar a otra persona.

210.2 La multa coercitiva es independiente de las sanciones que puedan imponerse con tal carácter y compatible con ellas.

(Texto según el artículo 199 de la Ley Nº 27444)

Artículo 211.- Compulsión sobre las personas
============================================

Los actos administrativos que impongan una obligación personalísima de no hacer o soportar, podrán ser ejecutados por compulsión sobre las personas en los casos en que la ley expresamente lo autorice, y siempre dentro del respeto debido a su dignidad y a los derechos reconocidos en la Constitución Política.

Si los actos fueran de cumplimiento personal, y no fueran ejecutados, darán lugar al pago de los daños y perjuicios que se produjeran, los que se deberán regular judicialmente.

(Texto según el artículo 200 de la Ley Nº 27444)

