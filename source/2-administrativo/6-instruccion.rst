=========================================
CAPÍTULO VI Instrucción del Procedimiento
=========================================

Artículo 170.- Actos de instrucción
===================================

170.1 Los actos de instrucción necesarios para la determinación, conocimiento y comprobación de los datos en virtud de los cuales deba pronunciarse la resolución, serán realizados de oficio por la autoridad a cuyo cargo se tramita el procedimiento de evaluación previa, sin perjuicio del derecho de los administrados a proponer actuaciones probatorias.

170.2 Queda prohibido realizar como actos de instrucción la solicitud rutinaria de informes previos, requerimientos de visaciones o cualquier otro acto que no aporte valor objetivo a lo actuado en el caso concreto, según su naturaleza.

(Texto según el artículo 159 de la Ley Nº 27444)

Artículo 171.- Acceso al expediente
===================================

171.1 Los administrados, sus representantes o su abogado, tienen derecho de acceso al expediente en cualquier momento de su trámite, así como a sus documentos, antecedentes, estudios, informes y dictámenes, obtener certificaciones de su estado y recabar copias de las piezas que contiene, previo pago del costo de las mismas. Sólo se exceptúan aquellas actuaciones, diligencias, informes o dictámenes que contienen información cuyo conocimiento pueda afectar su derecho a la intimidad personal o familiar y las que expresamente se excluyan por ley o por razones de seguridad nacional de acuerdo a lo establecido en el inciso 5) del artículo 2 de la Constitución Política. Adicionalmente se exceptúan las materias protegidas por el secreto bancario, tributario, comercial e industrial, así como todos aquellos documentos que impliquen un pronunciamiento previo por parte de la autoridad competente.

171.2 El pedido de acceso al expediente puede hacerse verbalmente, sin necesidad de solicitarlo mediante el procedimiento de transparencia y acceso a la información pública, siendo concedido de inmediato, sin necesidad de resolución expresa, en la oficina en que se encuentre el expediente, aunque no sea la unidad de recepción documental.

(Texto según el artículo 160 de la Ley Nº 27444, modificado según el artículo 2 Decreto Legislativo Nº 1272)

Artículo 172.- Alegaciones
==========================

172.1 Los administrados pueden en cualquier momento del procedimiento, formular alegaciones, aportar los documentos u otros elementos de juicio, los que serán analizados por la autoridad, al resolver.

172.2 En los procedimientos administrativos sancionadores, o en caso de actos de gravamen para el administrado, se dicta resolución sólo habiéndole otorgado un plazo perentorio no menor de cinco días para presentar sus alegatos o las correspondientes pruebas de descargo.

(Texto según el artículo 161 de la Ley Nº 27444)

Artículo 173.- Carga de la prueba
=================================

173.1 La carga de la prueba se rige por el principio de impulso de oficio establecido en la presente Ley.

173.2 Corresponde a los administrados aportar pruebas mediante la presentación de documentos e informes, proponer pericias, testimonios, inspecciones y demás diligencias permitidas, o aducir alegaciones.

(Texto según el artículo 162 de la Ley Nº 27444)

Artículo 174.- Actuación probatoria
===================================

174.1 Cuando la administración no tenga por ciertos los hechos alegados por los administrados o la naturaleza del procedimiento lo exija, la entidad dispone la actuación de prueba, siguiendo el criterio de concentración procesal, fijando un período que para el efecto no será menor de tres días ni mayor de quince, contados a partir de su planteamiento. Sólo podrá rechazar motivadamente los medios de prueba propuestos por el administrado, cuando no guarden relación con el fondo del asunto, sean improcedentes o innecesarios.

174.2 La autoridad administrativa notifica a los administrados, con anticipación no menor de tres días, la actuación de prueba, indicando el lugar, fecha y hora.

174.3 Las pruebas sobrevinientes pueden presentarse siempre que no se haya emitido resolución definitiva.(*)

(Texto según el artículo 163 de la Ley Nº 27444)

(*) De conformidad con el Numeral 17.1 del Artículo 17 de la Resolución Ministerial N° 0018-2020-MINAGRI, publicada el 18 enero 2020, concluido el plazo de cuatro (04) años para la ejecución del proyecto, el PEJEZA, a través de su Gerencia de Promoción de la Inversión Privada, realiza la inspección del predio, con la participación del adjudicatario, a quien deberá notificarse previamente de la realización de dicha actividad, de conformidad con lo señalado en el numeral 174.2 del presente artículo. La notificación debe efectuarse en el domicilio del adjudicatario señalado en la solicitud de acogimiento a lo dispuesto en el Decreto Supremo Nº 013-2018-MINAGRI.

Artículo 175.- Omisión de actuación probatoria
==============================================

Las entidades podrán prescindir de actuación de pruebas cuando decidan exclusivamente en base a los hechos planteados por las partes, si los tienen por ciertos y congruentes para su resolución.

(Texto según el artículo 164 de la Ley Nº 27444)

Artículo 176.- Hechos no sujetos a actuación probatoria
=======================================================

No será actuada prueba respecto a hechos públicos o notorios, respecto a hechos alegados por las partes cuya prueba consta en los archivos de la entidad, sobre los que se haya comprobado con ocasión del ejercicio de sus funciones, o sujetos a la presunción de veracidad, sin perjuicio de su fiscalización posterior.

(Texto según el artículo 165 de la Ley Nº 27444)

Artículo 177.- Medios de prueba
===============================

Los hechos invocados o que fueren conducentes para decidir un procedimiento podrán ser objeto de todos los medios de prueba necesarios, salvo aquellos prohibidos por disposición expresa. En particular, en el procedimiento administrativo procede:

1. Recabar antecedentes y documentos.

2. Solicitar informes y dictámenes de cualquier tipo.

3. Conceder audiencia a los administrados, interrogar testigos y peritos, o recabar de los mismos declaraciones por escrito.

4. Consultar documentos y actas.

5. Practicar inspecciones oculares.

(Texto según el artículo 166 de la Ley Nº 27444)

Artículo 178.- Solicitud de documentos a otras autoridades
==========================================================

178.1 La autoridad administrativa a la que corresponde la tramitación del asunto recabará de las autoridades directamente competentes los documentos preexistentes o antecedentes que estime conveniente para la resolución del asunto, sin suspender la tramitación del expediente.

178.2 Cuando la solicitud sea formulada por el administrado al instructor, deberá indicar la entidad donde obre la documentación y, si fuera de un expediente administrativo obrante en otra entidad, deberá acreditar indubitablemente su existencia.

(Texto según el artículo 167 de la Ley Nº 27444)

Artículo 179.- Presentación de documentos entre autoridades
===========================================================

179.1 Los documentos y antecedentes a que se refiere el artículo anterior deben ser remitidos directamente por quien es requerido dentro del plazo máximo de tres días, si se solicitaren dentro de la misma entidad, y de cinco, en los demás casos.

179.2 Si la autoridad requerida considerase necesario un plazo mayor, lo manifestará inmediatamente al requirente, con indicación del plazo que estime necesario, el cual no podrá exceder de diez días.

(Texto según el artículo 168 de la Ley Nº 27444)

Artículo 180.- Solicitud de pruebas a los administrados
=======================================================

180.1 La autoridad puede exigir a los administrados la comunicación de informaciones, la presentación de documentos o bienes, el sometimiento a inspecciones de sus bienes, así como su colaboración para la práctica de otros medios de prueba. Para el efecto se cursa el requerimiento mencionando la fecha, plazo, forma y condiciones para su cumplimiento.

180.2 Será legítimo el rechazo a la exigencia prevista en el párrafo anterior, cuando la sujeción implique: la violación al secreto profesional, una revelación prohibida por la ley, suponga directamente la revelación de hechos perseguibles practicados por el administrado, o afecte los derechos constitucionales. En ningún caso esta excepción ampara el falseamiento de los hechos o de la realidad.

180.3 El acogimiento a esta excepción será libremente apreciada por la autoridad conforme a las circunstancias del caso, sin que ello dispense al órgano administrativo de la búsqueda de los hechos ni de dictar la correspondiente resolución.

(Texto según el artículo 169 de la Ley Nº 27444)

Artículo 181.- Normativa supletoria
===================================

En lo no previsto en este apartado la prueba documental se regirá por los artículos 46 y 47.

(Texto según el artículo 170 de la Ley Nº 27444)

Artículo 182.- Presunción de la calidad de los informes
=======================================================

182.1 Los informes administrativos pueden ser obligatorios o facultativos y vinculantes o no vinculantes.

182.2 Los dictámenes e informes se presumirán facultativos y no vinculantes, con las excepciones de ley.

(Texto según el artículo 171 de la Ley Nº 27444)

Artículo 183.- Petición de informes
===================================

183.1 Las entidades sólo solicitan informes que sean preceptivos en la legislación o aquellos que juzguen absolutamente indispensables para el esclarecimiento de la cuestión a resolver. La solicitud debe indicar con precisión y claridad las cuestiones sobre las que se estime necesario su pronunciamiento.

183.2 La solicitud de informes o dictámenes legales es reservada exclusivamente para asuntos en que el fundamento jurídico de la pretensión sea razonablemente discutible, o los hechos sean controvertidos jurídicamente, y que tal situación no pueda ser dilucidada por el propio instructor.

183.3 El informante, dentro de los dos días de recibida, podrá devolver sin informe todo expediente en el que el pedido incumpla los párrafos anteriores, o cuando se aprecie que sólo se requiere confirmación de otros informes o de decisiones ya adoptadas.

(Texto según el artículo 172 de la Ley Nº 27444)

Artículo 184.- Presentación de informes
=======================================

184.1 Toda autoridad, cuando formule informes o proyectos de resoluciones fundamenta su opinión en forma sucinta y establece conclusiones expresas y claras sobre todas las cuestiones planteadas en la solicitud, y recomienda concretamente los cursos de acción a seguir, cuando éstos correspondan, suscribiéndolos con su firma habitual, consignando su nombre, apellido y cargo.

184.2 El informe o dictamen no incorpora a su texto el extracto de las actuaciones anteriores ni reitera datos que obren en expediente, pero referirá por su folio todo antecedente que permita ilustrar para su mejor resolución.

(Texto según el artículo 173 de la Ley Nº 27444)

Artículo 185.- Omisión de informe
=================================

185.1 De no recibirse el informe en el término señalado, la autoridad podrá alternativamente, según las circunstancias del caso y relación administrativa con el informante: prescindir del informe o citar al informante para que en fecha única y en una sesión, a la cual puede asistir el administrado, presente su parecer verbalmente, de la cual se elaborará acta que se adjuntará al expediente, sin perjuicio de la responsabilidad en que incurra el funcionario culpable de la demora.

185.2 La Ley puede establecer expresamente en procedimientos iniciados por los administrados que de no recibirse informes vinculantes en el plazo legal, se entienda que no existe objeción técnica o legal al planteamiento sometido a su parecer.

185.3 El informe presentado extemporáneamente puede ser considerado en la correspondiente resolución.

(Texto según el artículo 174 de la Ley Nº 27444)

Artículo 186.- Testigos
=======================

186.1 El proponente de la prueba de testigos tiene la carga de la comparecencia de los mismos en el lugar, fecha y hora fijados. Si el testigo no concurriera sin justa causa, se prescindirá de su testimonio.

186.2 La administración puede interrogar libremente a los testigos y, en caso de declaraciones contradictorias, podrá disponer careos, aun con los administrados.

(Texto según el artículo 175 de la Ley Nº 27444)

Artículo 187.- Peritaje
=======================

187.1 Los administrados pueden proponer la designación de peritos a su costa, debiendo en el mismo momento indicar los aspectos técnicos sobre los que éstos deben pronunciarse.

187.2 La administración se abstendrá de contratar peritos por su parte, debiendo solicitar informes técnicos de cualquier tipo a su personal o a las entidades técnicas aptas para dicho fin, preferentemente entre las facultades de las universidades públicas.

(Texto según el artículo 176 de la Ley Nº 27444)

Artículo 188.- Actuación probatoria de autoridades públicas
===========================================================

Las autoridades de entidades no prestan confesión, salvo en procedimientos internos de la administración; sin perjuicio de ser susceptibles de aportar elementos probatorios en calidad de testigos, informantes o peritos, si fuere el caso.

(Texto según el artículo 177 de la Ley Nº 27444)

Artículo 189.- Gastos de actuaciones probatorias
================================================

En el caso de que la actuación de pruebas propuestas por el administrado importe la realización de gastos que no deba soportar racionalmente la entidad, ésta podrá exigir el depósito anticipado de tales costos, con cargo a la liquidación final que el instructor practicará documentadamente al administrado, una vez realizada la probanza.

(Texto según el artículo 178 de la Ley Nº 27444)

Artículo 190.- Actuaciones probatorias que afecten a terceros
=============================================================

Los terceros tienen el deber de colaborar para la prueba de los hechos con respeto de sus derechos constitucionales.

(Texto según el artículo 179 de la Ley Nº 27444)

Artículo 191.- Proyecto de resolución
=====================================

Cuando fueren distintos la autoridad instructora de la competente para resolver, la instructora prepara un informe final en el cual recogerá los aspectos más relevantes del acto que lo promovió, así como un resumen del contenido de la instrucción, análisis de la prueba instruida, y formulará en su concordancia un proyecto de resolución.

(Texto según el artículo 180 de la Ley Nº 27444)

