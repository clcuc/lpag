# Administrative Procedure Law
https://tommiepat.gitlab.io/lpag/  

This repository contains the LPAG or Administrative Procedure Law of Peru.  

## Building
To build the website, `git clone` this repository and then, on your terminal, follow the commands
shown in the `gitlab-ci.yml` file. A virtual environment like `virtualenv` or `conda` is
recommended.  

## Tools
Some tools used in this repository:  

- Python
- Sphinx-doc with reStructuredText
- ReadTheDocs theme
